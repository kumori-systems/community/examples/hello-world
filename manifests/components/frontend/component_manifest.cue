package component

// In Kumori Platform a microservice is represented by a set of executions
// ("instances" or "replicas") of a Kumori Component, all of them equally configured.
// This 'Artifact' describes the Kumori Component.
#Artifact: {

  // Manifest reference name
  ref: name:  "components/frontend"

  description: {

    // Connectivity of a component: the set of channels it exposes.
    srv: {
      // Server channels: functionality provided by the component through an endpoint.
      server: {
        restapi: { protocol: "http", port: 8080 }
      }
      // Client channels: dependency on some other component.
      client: {}
      // Duplex channels: channels code both a client and a server channel,
      // modeling endpoints used to initiate requests as well as serve them.
      // (useful in scenarios where a group of instances must carry out complex
      // coordination protocols (e.g., consensus), and each one of those instances
      // plays both a "client" and a "server" role.
      duplex: {}
    }

    // Component configuration (no configuration in this example).
    config: {
      // Configuration parameter data that a component can consume.
      parameter: {}
      // Dictionary of resources (persistent volumes, secrets) used by the component.
      resource: {}
    }

    // Amount of machine resources given to each running instance of the
    // component (vertical size).
    size: {
      bandwidth: { size: 10, unit: "M" }
    }

    // Component probes
    probe: frontend: {
      // A liveness probe is intended for the platform to find if an instance is alive
      liveness: {
        protocol: http : {
          port: srv.server.restapi.port
          path: "/health"
        }
        startupGraceWindow: {
          unit: "ms",
          duration: 20000,
          probe: true
        }
        frequency: "medium"
        timeout: 30000  // msec
      }
      // A readiness probe is intended for the platform to find if an instance
      // is ready to receive work or not (backpresure mechanism)
      readiness: {
        protocol: http : {
          port: srv.server.restapi.port
          path: "/health"
        }
        frequency: "medium"
        timeout: 30000 // msec
      }
    }

    // Component code: docker images and configuration mapping.
    // A component’s code may consist of more than one image, each of which
    // should be ran within its own container.
    code: {

      // Only one container, in this case.
      frontend: {
        name: "frontend"

        // Docker image
        image: {
          // Docker registry
          hub: {
            // "docker.io" is the default value
            name: ""
            // In this case a public image is used: credentials (secret) not required
            secret: ""
          }
          // Image name
          tag: "kumoripublic/examples-hello-world-frontend:v1.0.6"
        }

        // Maps parts of the component configuration to content that can be
        // placed in files and environment variables so that code running can
        // access its configured parameters.
        mapping: {

          // Filesystem mappings are organizad as a map of mappings. Each
          // element maps  a file or, potentially, a tree of folders and files.
          filesystem: {}

          // The environment mapping is organized as a dictionary of environment
          // variables, whose values will be made available to the container on execution.
          env: {
            // The web server of component 'frontend' uses the TCP port specified
            // in HTTP_SERVER_PORT_ENV environment variable, so it is injected.
            // 'strconv' CUE package includes the FromtUint function, that returns
            // the string representation of i in the given base
            HTTP_SERVER_PORT_ENV: value: "\(srv.server.restapi.port)"
          }
        }

        // Amount of machine resources given to each container of each running
        // instance of the component (vertical size).
        size: {
          memory: { size: 100, unit: "M" }
          mincpu: 100
          cpu: { size: 200, unit: "m" }
        }
      }
    }

  }
}
