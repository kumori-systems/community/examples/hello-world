package service

import (
  // Kumori Component used in this Kumori Service Application
  f ".../components/frontend:component"
)

// In Kumori Platform, a Service Application represents a set of interconnected
// Kumori Components working together.
// 'ServiceArtifact' describes the Kumori Service Application.
#Artifact: {
  ref: name:  "service"

  description: {

    //
    // Kumori Component roles and configuration
    //

    // Configuration (parameters and resources) to be provided to the Kumori
    // Service Application.
    // (no configuration in this example)
    config: {
      parameter: {}
      resource: {}
    }

    // List of Kumori Components of the Kumori Service Application.
    // In a service, the same component could be run playing multiple roles, with
    // different purposes, each of them with its own configuration.
    // In this case there is only one role ("frontend") for only one component
    // described in the imported component manifest.
    role: {
      frontend: {
        artifact: f.#Artifact
        // Configuration spread: computing each role configuration based on service
        // configuration (no configuration in this example).
        config: {
          parameter: {}
          resource: {}
          resilience: description.config.resilience
        }
      }
    }

    //
    // Kumori Service topology: how roles are interconnected
    //

    // Connectivity of a service application: the set of channels it exposes.
    srv: {
      // Server channels: functionality provided by the service through an endpoint.
      server: {
        hello: { protocol: "http", port: 80 }
      }
    }

    // Connectors specify the topology graph.
    // In this case, there is only one load-balancer connector connecting the
    // service channel and the role channel
    connect: {
      // Outside -> FrontEnd (LB connector)
      cinbound: {
        as: "lb"
        from: self: "hello"
        to: frontend: "restapi": _
      }
    }
  }
}
