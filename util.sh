#!/bin/bash

# Cluster variables
CLUSTERNAME="..."
REFERENCEDOMAIN="test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

# Service variables
INBOUNDNAME="helloworldinb"
DEPLOYNAME="helloworlddep"
DOMAIN="helloworlddomain"
SERVICEURL="helloworld-${CLUSTERNAME}.${REFERENCEDOMAIN}"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"

# Change if special binaries want to be used
KAM_CMD="kam"
KAM_CTL_CMD="kam ctl"

case $1 in

'refresh-dependencies')
  cd manifests
  # Dependencies are removed and added again just to ensure that the right versions
  # are used.
  # This is not necessary, if the versions do not change!
  ${KAM_CMD} mod dependency --delete kumori.systems/kumori
  ${KAM_CMD} mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  # Just for sanitize: clean the directory containing dependencies
  rm -rf ./cue.mod
  cd ..
  ;;

'create-domain')
  ${KAM_CTL_CMD} register domain $DOMAIN --domain $SERVICEURL
  ;;

'deploy-inbound')
  ${KAM_CTL_CMD} register inbound $INBOUNDNAME \
    --domain $DOMAIN \
    --cert $CLUSTERCERT
  ;;

# To check if our module is correct (from the point of view of the CUE syntax and
# the Kumori service model), we can use "kam process" to enerate the JSON
# representation
'dry-run')
  ${KAM_CMD} process deployment -t ./manifests
  ;;


'deploy-service')
  ${KAM_CMD} service deploy -d deployment -t ./manifests $DEPLOYNAME -- \
    --comment "Hello world service" \
    --wait 5m
  ;;

# Currently, "kam service update" is not reporting changes to used ("kumorictl update"
# command do it) -- https://gitlab.com/kumori/project-management/-/issues/1440#note_1547945612
'update-service')
  ${KAM_CMD} service update -d deployment -t ./manifests $DEPLOYNAME -- \
    --comment "Updating hello world service" \
    --wait 5m
  ;;

'link')
  ${KAM_CTL_CMD} link $DEPLOYNAME:hello $INBOUNDNAME:inbound
  ;;

'deploy-all')
  $0 create-domain
  $0 deploy-inbound
  $0 deploy-service
  $0 link
  ;;

'describe')
  ${KAM_CMD} service describe $DEPLOYNAME
  ;;

# Test the hello-world service
'test')
  curl https://${SERVICEURL}/hello
  ;;

# Undeploy all
'undeploy-all')
  ${KAM_CTL_CMD} unlink $DEPLOYNAME:hello $INBOUNDNAME:inbound --wait 5m
  ${KAM_CMD} service undeploy $DEPLOYNAME -- --wait 5m
  ${KAM_CMD} service undeploy $INBOUNDNAME -- --wait 5m
  ${KAM_CTL_CMD} unregister domain $DOMAIN
  ;;

*)
  echo "This script doesn't contain that command"
	;;

esac
