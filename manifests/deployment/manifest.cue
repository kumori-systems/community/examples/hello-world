package deployment
// If a package name other than "deployment" is used, then the
// "kumorictl register deployment" operation must include the "--package" flag.

import (
  // Kumori Service Appication to be deployed.
  s ".../service:service"
)

// A Kumori Service Application is deployed as a service within a Kumori Platform.
// '#Deployment' describes the configuration to be used.
#Deployment: {
  name: "helloworlddep"

  // Imported service aplication manifest to be deployed
  artifact: s.#Artifact

  config: {

    // Configuration (parameters and resources) to be injected (no configuration
    // in this example)
    // All configuration parameters of the service application must be set to
    // concrete values.
    parameter: {}
    resource: {}

    // Horizontal size: in this case, just the number of instances of the
    // frontend role, and service resilience to failures
    scale: detail: {
      frontend: hsize: 3
    }
    resilience: 1
  }
}
