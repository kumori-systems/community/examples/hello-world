/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

const express = require('express')

var hostname = '0.0.0.0'
var port = process.env.HTTP_SERVER_PORT_ENV

let app = express()

app.get('/hello', (req, res) => {
  console.log('New hello request received')
  res.send('Hello World')
})
app.get('/health', (req, res) => {
  console.log('New health request received')
  res.send('OK')
})
app.use((req, res, next) => {
  res.status(404).send('Not found')
})

app.listen(port, () => {
  console.log(`Server running at http://${hostname}:${port}/`)
})

